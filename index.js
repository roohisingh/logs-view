const http = require('http');
const fs = require('fs');

const logFilePath = './logFile.txt';

const server = http.createServer((req, res) => {
  if (req.method === 'GET' && req.url.startsWith('/logs')) {
    const timestampParam = req.url.split('/logs?timeStamp=')[1]
    const requestedTimestamp = new Date(timestampParam);

    // Read the log file asynchronously
    fs.open(logFilePath, 'r', (err, fd) => {
      if (err) {
        res.writeHead(500, { 'Content-Type': 'text/plain' });
        res.end('Internal Server Error');
        return;
      }

      const buffer = Buffer.alloc(256); // each log line is up to 256 bytes
      let logs = '';
      let bytesRead = 0;

      // Function to read and send logs
      function readLogs() {
        fs.read(fd, buffer, 0, buffer.length, bytesRead, (err, bytes, bytesRead) => {
          if (err) {
            fs.close(fd, () => {}); // Close the file descriptor
            res.writeHead(500, { 'Content-Type': 'text/plain' });
            res.end('Internal Server Error');
            return;
          }

          if (bytes === 0) {
            // End of file
            fs.close(fd, () => {}); // Close the file descriptor
            res.writeHead(200, { 'Content-Type': 'text/plain' });
            res.end(logs);
          } else {
            const logLine = buffer.toString('utf8', 0, bytes);
            console.log('================', logLine)
            // each log line starts with a timestamp
            const logTimestamp = new Date(logLine.split(' ')[0]);
            console.log('------------------', logTimestamp)

            if (logTimestamp >= requestedTimestamp) {
              logs += logLine;
            }
            // Continue reading the file
            readLogs();
          }
        });
      }

      readLogs(); // Start reading the logs
    });
  } else {
    res.writeHead(404, { 'Content-Type': 'text/plain' });
    res.end('Not Found');
  }
});

const port = 3000;
server.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
